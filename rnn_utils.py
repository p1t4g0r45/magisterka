from __future__ import print_function
import numpy as np
import os
from collections import deque
from random import randint, shuffle
from sklearn.preprocessing import normalize



def shift(key, array):
    a = deque(array) # turn list into deque
    a.rotate(key)    # rotate deque by key
    return list(a)   # turn deque back into a list


# method for generating text
def generate_text(model, seq_length, length, vocab_size, ix_to_char, category, categories_len):
    if seq_length > length:
        raise Exception("generate length must be bigger than seq length")
    # starting with random character
    letters_length = vocab_size - categories_len
    ix = np.random.randint(vocab_size)
    y_char = [ix_to_char.get(ix, "_")]
    X = np.zeros((1, seq_length + 1, vocab_size))  # + 1 for category
    X[0, 0, vocab_size - category - 1] = 1  # category is set as first char (category number is placed as a whole vector of length vocab_size)
    print("starting generating")
    for i in range(1, seq_length + 1):  # until we haven't generated a length of seq_length
        # appending the last predicted character to sequence
        X[0, i, :][ix] = 1
        X[0, i, :][vocab_size - category - 1] = 1  # set category char for each char
        predicted = model.predict(X[:, :i + 1, :])[0]
        last_letter_predicted = predicted[-1][:letters_length]
        normalized = last_letter_predicted / last_letter_predicted.sum(axis = 0, keepdims = 1)
        ix = np.random.choice(range(len(normalized)), p=normalized)
        y_char.append(ix_to_char.get(ix, "_"))

    remaining_length = length - seq_length
    while remaining_length > 0:  # seq_length reached, we drop the least meaningful char at the beginning
        remaining_length -= 1
        X[0] = shift(seq_length, X[0])
        X[0, 0, vocab_size - category - 1] = 1  # category has to be set every time
        X[0, seq_length] = 0  # clear last char
        X[0, seq_length, :][ix] = 1  # set last char
        predicted = model.predict(X[:, :seq_length + 1, :])[0]
        last_letter_predicted = predicted[-1][:letters_length]
        normalized = last_letter_predicted / last_letter_predicted.sum(axis = 0, keepdims = 1)
        ix = np.random.choice(range(len(normalized)), p=normalized)
        y_char.append(ix_to_char.get(ix, "_"))

    print(('').join(y_char))
    return ('').join(y_char)


def initial_load(data_dir, onlyFile, seq_length):
    data = dict()  # category (filename) -> content (few MB string)
    charset = list()  # all unique chars (typically around 100)
    for filename in os.listdir(data_dir):
        if filename == onlyFile or onlyFile == 'all':
            file_data = open(data_dir + "/" + filename, 'r').read()
            file_chars = list(set(file_data))
            data[filename] = file_data
            charset.append(file_chars)

    length = min([len(data[name]) for name in data])
    length = length - length % seq_length + 1
    print("setting length of all files to " + str(length))
    data = {name: data[name][:length] for name in data}
    chars = list(set([item for sublist in charset for item in sublist]))
    print(chars)
    vocab_size = len(chars)
    ix_to_char = {ix: char for ix, char in enumerate(chars)}
    char_to_ix = {char: ix for ix, char in enumerate(chars)}
    categories = {name: ix for ix, name in enumerate(data)}  # set next ID's for categories
    for c in categories:
        ix_to_char[vocab_size + categories[c]] = '_'

    print('{} categories'.format(len(data)))
    for category in data:
        print('category {}. {} length: {} characters'.format(categories[category], category, len(data[category])))
    print('Vocabulary size: {} characters'.format(vocab_size))
    return data, vocab_size, ix_to_char, char_to_ix, categories


# method for preparing the training data
def load_data(data, vocab_size, char_to_ix, seq_length, begin_index, seq_num, category):

    sub_data = data[begin_index:begin_index + seq_length * seq_num + 1]  # get batch from the input text
    seq_num = int((len(sub_data) -1)/ seq_length)  # last batch is smaller
    if seq_num == 0:  # corner case
        seq_num = 1
        seq_length = len(sub_data) - 1
    X = np.zeros((seq_num, seq_length + 1, vocab_size))  # + 1 for category
    y = np.zeros((seq_num, seq_length + 1, vocab_size))  # size hase to be the same, but I don't fill the category,
    # because when genereting I look for the biggest probability
    for i in range(0, seq_num):
        X_sequence = sub_data[i * seq_length:(i + 1) * seq_length]
        X_sequence_ix = [char_to_ix[value] for value in X_sequence]
        input_sequence = np.zeros((seq_length + 1, vocab_size))
        for j in range(seq_length):
            input_sequence[j + 1][X_sequence_ix[j]] = 1.
            input_sequence[j + 1][vocab_size - category - 1] = 1.  # set category char for each char
        input_sequence[0][vocab_size - category - 1] = 1  # set category as first char
        X[i] = input_sequence

        y_sequence = sub_data[i * seq_length + 1:(i + 1) * seq_length + 1]
        y_sequence_ix = [char_to_ix[value] for value in y_sequence]
        target_sequence = np.zeros((seq_length + 1, vocab_size))
        for j in range(seq_length):
            target_sequence[j + 1][y_sequence_ix[j]] = 1.
        # target_sequence[0] = category - left out on purpose
        y[i] = target_sequence
    return X, y


def create_empty_batch(batch_size, seq_length, vocab_size):
    X_batched = np.zeros((batch_size, seq_length + 1, vocab_size))
    y_batched = np.zeros((batch_size, seq_length + 1, vocab_size))
    return X_batched, y_batched


def get_all_possibilities(data, seq_num, seq_length):
    begin_index = 0
    possibilities = []
    while begin_index < len(list(data.values())[0]):
        for file in data:
            possibilities.append((file, begin_index))
        begin_index = begin_index + seq_num * seq_length
    shuffle(possibilities)
    return possibilities


def read_file_part(data_dir, start_index, end_index):
    with open(data_dir) as fin:
        fin.seek(start_index)
        return fin.read(end_index - start_index)
