#!/bin/bash

function list_include_item {
  local list="$1"
  local item="$2"
  if [[ $list =~ (^|[[:space:]])"$item"($|[[:space:]]) ]] ; then
    # yes, list include item
    result=0
  else
    result=1
  fi
  return $result
}

mkdir "3.split"
mkdir "3.split/classify"
mkdir "3.split/generate"
mkdir "3.split/classify/2.without_chars_headers"
mkdir "3.split/generate/2.without_chars_headers"

for dir in "2.without_chars_headers"/*; do
  mkdir "3.split/classify/"$dir
  mkdir "3.split/generate/"$dir
  FILES=`ls $dir | wc -l`
  M=`expr $FILES "*" 2 / 10`
  echo $M
  CLASSIFY=`ls $dir | sort -R | tail -$M`
  echo $CLASSIFY
  for file in $dir/*; do
    FILENAME=`echo $file | sed 's/.*\///'`
    if `list_include_item "$CLASSIFY" "$FILENAME"`; then
      cp $file "3.split/classify/"$file
    else
      cp $file "3.split/generate/"$file
    fi
  done
done


