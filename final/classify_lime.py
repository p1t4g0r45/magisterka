
from __future__ import division
from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.linear_model import SGDClassifier
from sklearn.pipeline import Pipeline
from sklearn import metrics
from sklearn.datasets import load_files
from sklearn.cross_validation import train_test_split
import numpy as np
from random import randint, shuffle
from lime import lime_text
from sklearn.pipeline import make_pipeline
from lime.lime_text import LimeTextExplainer
from lime.lime_base import LimeBase
import sklearn
from sklearn.naive_bayes import MultinomialNB





# linear SVM
def SVM():
    # load data
    categories = ['comp.graphics', 'rec.sport.baseball', 'sci.med', 'sci.space', 'alt.atheism']

    with open("out.txt", "a") as myfile:
        newsgroups_test = load_files('5.generated_from_80_5_4', categories=categories)
        newsgroups_train = load_files('3.split/classify', categories=categories)
        X_train = newsgroups_train.data
        y_train = newsgroups_train.target
        X_test = newsgroups_test.data
        y_test = newsgroups_test.target

        class_names = [x.split('.')[-1] if 'misc' not in x else '.'.join(x.split('.')[-2:]) for x in
                       newsgroups_train.target_names]
        # pipeline (tokenizer => transformer => linear SVM classifier)

        vectorizer = sklearn.feature_extraction.text.TfidfVectorizer(lowercase=False)
        train_vectors = vectorizer.fit_transform(newsgroups_train.data)
        test_vectors = vectorizer.transform(newsgroups_test.data)

        nb = MultinomialNB(alpha=.01)
        nb.fit(train_vectors, newsgroups_train.target)

        predicted = nb.predict(test_vectors)
        sklearn.metrics.f1_score(newsgroups_test.target, predicted, average='weighted')

        c = make_pipeline(vectorizer, nb)

        # evaluate on test set
        print("Accuracy : {}%".format(np.mean(predicted == y_test)*100))
        print(metrics.classification_report(y_test, predicted, target_names=newsgroups_test.target_names))
        print("Confusion Matrix : \n", metrics.confusion_matrix(y_test, predicted))

        explainer = LimeTextExplainer(class_names=class_names)

        idx = 142
        print(newsgroups_test.data[idx])
        exp = explainer.explain_instance(newsgroups_test.data[idx], c.predict_proba, num_features=6, labels=[0, 1, 2, 3, 4])
        print('Document id: %d' % idx)
        print('Predicted class =', class_names[nb.predict(test_vectors[idx]).reshape(1, -1)[0, 0]])
        print('True class: %s' % class_names[newsgroups_test.target[idx]])

        print ('Explanation for class %s' % class_names[0])
        print ('\n'.join(map(str, exp.as_list(label=0))))
        print ()
        print ('Explanation for class %s' % class_names[1])
        print ('\n'.join(map(str, exp.as_list(label=1))))
        print ()
        print ('Explanation for class %s' % class_names[2])
        print ('\n'.join(map(str, exp.as_list(label=2))))
        print ()
        print ('Explanation for class %s' % class_names[3])
        print ('\n'.join(map(str, exp.as_list(label=3))))
        print ()
        print ('Explanation for class %s' % class_names[4])
        print ('\n'.join(map(str, exp.as_list(label=4))))

        exp = explainer.explain_instance(newsgroups_test.data[idx], c.predict_proba, num_features=6, top_labels=2)
        print(exp.available_labels())

        exp.save_to_file("file1", text=False)
        exp.save_to_file("file2", text=newsgroups_test.data[idx], labels=(0,))


SVM()
