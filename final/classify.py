
from __future__ import division
from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.linear_model import SGDClassifier
from sklearn.pipeline import Pipeline
from sklearn import metrics
from sklearn.datasets import load_files
from sklearn.cross_validation import train_test_split
import numpy as np
from random import randint, shuffle
import codecs
import pickle


# linear SVM
def SVM():
    # load data
    categories = ['comp.graphics', 'rec.sport.baseball', 'sci.med', 'sci.space', 'alt.atheism']

    with open("out.txt", "a") as myfile:
        generated = load_files('5.generated_from_80_5_4', categories=categories)
        to_learn = load_files('3.split/classify', categories=categories)
        X_train = to_learn.data
        y_train = to_learn.target
        X_test = generated.data
        y_test = generated.target

        # pipeline (tokenizer => transformer => linear SVM classifier)
        text_clf = Pipeline([('vect', CountVectorizer()),('tfidf', TfidfTransformer()),('clf', SGDClassifier(loss='hinge', penalty='l2',alpha=1e-3, n_iter=5, random_state=42))])
        _ = text_clf.fit(X_train, y_train)

        # evaluate on test set
        predicted = text_clf.predict(X_test)
        print("Accuracy : {}%".format(np.mean(predicted == y_test)*100))
        print(metrics.classification_report(y_test, predicted, target_names=generated.target_names))
        print("Confusion Matrix : \n", metrics.confusion_matrix(y_test, predicted))

        print(predicted)

SVM()