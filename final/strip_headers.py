import codecs
from sklearn.datasets import twenty_newsgroups
import os

data_dir = "2.without_chars"
out_data_dir = "2.without_chars_headers"
if not os.path.exists(out_data_dir):
    os.makedirs(out_data_dir)

for dirname in os.listdir(data_dir):
    if not os.path.exists("{}/{}".format(out_data_dir, dirname)):
        os.makedirs("{}/{}".format(out_data_dir, dirname))
    for filename in os.listdir("{}/{}".format(data_dir, dirname)):
        input_filepath = "{}/{}/{}".format(data_dir, dirname, filename)
        input_file_data = codecs.open(input_filepath, 'r', encoding='utf-8')
        text = twenty_newsgroups.strip_newsgroup_header(input_file_data.read())
        text = twenty_newsgroups.strip_newsgroup_footer(text)
        input_file_data.close()

        output_filepath = "{}/{}/{}".format(out_data_dir, dirname, filename)
        output_file_data = codecs.open(output_filepath, 'w', encoding='utf-8')
        output_file_data.write(text)
        output_file_data.close()