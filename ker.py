#!/usr/bin/env python

from __future__ import print_function
import matplotlib.pyplot as plt
import numpy as np
import time
import csv
from keras.models import Sequential
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM, SimpleRNN
from keras.layers import CuDNNLSTM
from keras.layers.wrappers import TimeDistributed
import argparse
from rnn_utils import *
import codecs
import datetime, time

# Parsing arguments for Network definition
ap = argparse.ArgumentParser()
ap.add_argument('-data_dir', default='./test')
ap.add_argument('-batch_size', type=int, default=50)
ap.add_argument('-layer_num', type=int, default=2)
ap.add_argument('-seq_length', type=int, default=100)
ap.add_argument('-seq_num', default=1)  # seq_num * seq_length trained at once
ap.add_argument('-hidden_dim', type=int, default=500)
ap.add_argument('-generate_length', type=int, default=500)
ap.add_argument('-nb_epoch', type=int, default=20)  # unused
ap.add_argument('-mode', default='train')
ap.add_argument('-weights', default='')
ap.add_argument('-category', default='')
ap.add_argument('-only', default='all')
ap.add_argument('-out', default='checkpoints')
ap.add_argument('-amount', default=1)
args = vars(ap.parse_args())

DATA_DIR = args['data_dir']
BATCH_SIZE = args['batch_size']
HIDDEN_DIM = args['hidden_dim']
SEQ_LENGTH = int(args['seq_length'])
SEQ_NUM = int(args['seq_num'])
WEIGHTS = args['weights']
CATEGORY = args['category']
ONLY = args['only']
OUT = args['out']
AMOUNT = args['amount']

GENERATE_LENGTH = args['generate_length']
LAYER_NUM = args['layer_num']

data, VOCAB_SIZE, ix_to_char, char_to_ix, categories = initial_load(DATA_DIR, ONLY, SEQ_LENGTH)
VOCAB_SIZE = VOCAB_SIZE + len(categories)

# Creating and compiling the Network
model = Sequential()
model.add(CuDNNLSTM(HIDDEN_DIM, input_shape=(None, VOCAB_SIZE), return_sequences=True))
for i in range(LAYER_NUM - 1):
    model.add(CuDNNLSTM(HIDDEN_DIM, return_sequences=True))
model.add(TimeDistributed(Dense(VOCAB_SIZE)))
model.add(Activation('softmax'))
model.compile(loss="categorical_crossentropy", optimizer="rmsprop")

print("generate")
# Generate some sample before training to know how bad it is!
#generate_text(model, GENERATE_LENGTH, VOCAB_SIZE, ix_to_char)

if not WEIGHTS == '':
    model.load_weights(WEIGHTS)
    nb_epoch = int(WEIGHTS[WEIGHTS.rfind('_') + 1:WEIGHTS.find('.')])
else:
    nb_epoch = 0

# Training if there is no trained weights specified
if args['mode'] == 'train' or WEIGHTS == '':
    # Creating training data
    if not os.path.exists(OUT):  # dir for storing weights
        os.makedirs(OUT)
    outfile = open('{}/chars'.format(OUT), 'w')
    outfile.write(">".join(ix_to_char.values()))
    outfile.close()
    outfile = open('{}/categories'.format(OUT), 'w')
    outfile.write(">".join(categories.keys()))
    outfile.close()
    import datetime, time

    ts = time.time()
    st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
    print(st)
    while True:
        print('\n\nEpoch: {}\n'.format(nb_epoch))
        possibilities = get_all_possibilities(data, SEQ_NUM, SEQ_LENGTH)  # randomized inputs
        X_batched, y_batched = create_empty_batch(BATCH_SIZE, SEQ_LENGTH, VOCAB_SIZE)
        for index, possibility in enumerate(possibilities):
            file = possibility[0]
            begin_index = possibility[1]
            X, y = load_data(data[file], VOCAB_SIZE, char_to_ix, SEQ_LENGTH, begin_index, SEQ_NUM, categories[file])
            X_batched[index % BATCH_SIZE] = X[0]
            y_batched[index % BATCH_SIZE] = y[0]
            if index % BATCH_SIZE == BATCH_SIZE - 1:
                model.fit(X_batched, y_batched, batch_size=BATCH_SIZE, verbose=1, nb_epoch=1)
                X_batched, y_batched = create_empty_batch(BATCH_SIZE, SEQ_LENGTH, VOCAB_SIZE)
        nb_epoch += 1
        ts = time.time()
        st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
        print(st)
        break
        print("Generating:")
        for file in data:  # generate sample for each category
            print(file + ": ")
            generate_text(model, SEQ_LENGTH, GENERATE_LENGTH, VOCAB_SIZE, ix_to_char, categories[file], len(categories))
        if nb_epoch % 10 == 0:
            model.save_weights(OUT + "/" + 'checkpoint_layer_{}_hidden_{}_epoch_{}.hdf5'.format(LAYER_NUM, HIDDEN_DIM, nb_epoch))

# unused
# Else, loading the trained weights and performing generation only
elif WEIGHTS != '':
    # Loading the trained weights
    charfile = codecs.open('{}/chars'.format(OUT), encoding='utf-8')
    chars = charfile.read().split(">")
    ix_to_char = {ix: char for ix, char in enumerate(chars)}
    for c in categories:
        ix_to_char[VOCAB_SIZE + categories[c]] = '_'

    categoriesfile = codecs.open('{}/categories'.format(OUT), encoding='utf-8')
    cats = categoriesfile.read().split(">")
    categories = {cat: ix for ix, cat in enumerate(cats)}
    print("loaded: {}".format(ix_to_char))
    model.load_weights(WEIGHTS)
    if not os.path.exists("{}/gen".format(OUT)):
        os.makedirs("{}/gen".format(OUT))
    if CATEGORY != '':
        generate_text(model, SEQ_LENGTH, GENERATE_LENGTH, VOCAB_SIZE, ix_to_char, int(CATEGORY), len(categories))
    else:
        for category in categories:
            if not os.path.exists("{}/gen/{}".format(OUT, category)):
                os.makedirs("{}/gen/{}".format(OUT, category))
            print("\n\nGenerating for category: {}.{}\n\n".format(categories[category], category))
            for i in range(int(AMOUNT)):
                outfile = open('{}/gen/{}/{}'.format(OUT, category, i), 'w')
                print("\nNUMBER: {}\n".format(i))
                text = generate_text(model, SEQ_LENGTH, GENERATE_LENGTH, VOCAB_SIZE, ix_to_char, categories[category], len(categories))
                outfile.write(text)
                outfile.close()
    print('\n\n')
else:
    print('\n\nNothing to do!')
